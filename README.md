################################################

# Mission Demolition 

# Jayden McCowin

# 1/25/2023

################################################

___ What are the controls to your game? How do we play? Use the mouse on the slingshot to aim towards the goal.

___ What creative additions did you make? How can we find them? I made 4 different castles to try to destroy and get to the end of the level. I made a main menu and a quit button.

___ Any assets used that you didn’t create yourself? (art, music, etc. Just tell us where you got it, link it here) N/A

___ Did you receive help from anyone outside this class? (list their names and what they helped with) N/A

___ Did you get help from any AI Code Assistants? (Tell us which .cs file to look in for the citation and describe what you learned) N/A

___ Did you get help from any online websites, videos, or tutorials? (link them here) N/A

___ What trouble did you have with this project? Nothing

___ Is there anything else we should know? No
